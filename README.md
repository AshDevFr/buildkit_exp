# GITLAB CI + BUILDKIT

## Cache issue 

After building an image using the cache with `--cache-from` the image pushed to the registry is missing some layers.  
It only happens when using the cache.
