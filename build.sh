#!/bin/sh

set -ex

docker pull $IMAGE_NAME && CACHE_FLAGS="--cache-from $IMAGE_NAME"

if [ -z "$CACHE_FLAGS" ]; then
  docker pull $BRANCH_IMAGE_NAME && CACHE_FLAGS="--cache-from $BRANCH_IMAGE_NAME"
fi

if [ -z "$CACHE_FLAGS" ]; then
  MASTER_IMAGE_NAME="$CI_REGISTRY_IMAGE:master"
  docker pull $MASTER_IMAGE_NAME && CACHE_FLAGS="--cache-from $MASTER_IMAGE_NAME"
fi

docker build --progress=plain \
  --build-arg ALPINE_VERSION=$ALPINE_VERSION \
  --build-arg SOME_VAR=foo \
  --build-arg BUILDKIT_INLINE_CACHE=1 \
  -t $IMAGE_NAME -t $BRANCH_IMAGE_NAME \
  $CACHE_FLAGS \
  .

docker push $IMAGE_NAME
docker push $BRANCH_IMAGE_NAME
