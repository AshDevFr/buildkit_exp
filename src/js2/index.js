const _ = require('lodash');
const assert = require('assert').strict;

console.log("Asserts: Start");
assert.equal(_.head([1, 2, 3]), 1);
assert.notEqual(_.head([1, 2, 3]), 2);
console.log("Asserts: End");
