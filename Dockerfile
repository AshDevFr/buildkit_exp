ARG ALPINE_VERSION=3.12.0
FROM alpine:$ALPINE_VERSION as base

ENV WORKDIR /opt/app
WORKDIR $WORKDIR

# Add dependencies
RUN apk update && \
  apk upgrade --no-cache && \
  apk add --no-cache \
    nodejs \
    yarn \
    git \
    bash

COPY src/js1/package.json src/js1/yarn.lock js1/
WORKDIR $WORKDIR/js1
RUN yarn

WORKDIR $WORKDIR

COPY src/js2/package.json src/js2/yarn.lock js2/
WORKDIR $WORKDIR/js2
RUN yarn

WORKDIR $WORKDIR

ARG SOME_VAR
RUN echo "$SOME_VAR" > tmp.txt

FROM base

COPY src/js1 js1/
COPY src/js2 js2/

COPY src/ .
